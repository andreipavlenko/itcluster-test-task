document.querySelectorAll('.dropdown-list > li').forEach(el => {
  el.addEventListener('mouseenter', e => {
    var dropdownContent = e.target.children.item(1);
    noOffscreenDropdown(dropdownContent);
  });
});

function noOffscreenDropdown(dropdown) {
  var bounds = dropdown.getBoundingClientRect();
  if (bounds.top < 0) {
    dropdown.classList.add('dropdown-bottom');
  } else if (bounds.bottom > window.innerHeight) {
    dropdown.classList.add('dropdown-top');
  }
}
